from ui.pages.products_page import ProductPage
from ui.pages.cart_page import CartPage
from ui.pages.checkout_page import CheckoutPage
from ui.pages.authorization_page import AuthorizationPage

current_url_after_authorization = "https://www.saucedemo.com/inventory.html"

"""
1. Открыть браузер и развернуть на весь экран.
2. Перейти на сайт https://www.saucedemo.com/.
3. В поле ввода "Username" ввести "standard_user или performance_glitch_user".
4. В поле ввода "Password" ввести "secret_sauce".
5. Убедиться в авторизации пользователя.
6. Открыть карточку первого товара.
6. Добавить товар в корзину.
7. Перейти в корзину.
8. Убидиться что товар добавлен в корзину.
9. Перейти к оформлению заказа.
10. Ввести имя,фамилию и почтовый код в поля ввода 'First Name','Last Name', 'Zip/Postal Code'
11. Нажать кнопку "Continue".
12. Завершить заказ.
13. Проверить что заказ успешно завершен.
14. Закрыть браузер.
"""

def test_checkout(driver,user):
    driver.get("https://www.saucedemo.com/")
    authorization_page = AuthorizationPage(driver)
    authorization_page.enter_user_name(user)
    authorization_page.enter_password()
    authorization_page.click_login_button()
    get_current_url = driver.current_url
    assert get_current_url == current_url_after_authorization
    product_page = ProductPage(driver)
    product_page.open_first_product_on_the_page()
    product_page.click_add_to_cart_in_the_product_card()
    product_page.go_to_shopping_cart()
    cart_page = CartPage(driver)
    product_availability_in_cart = cart_page.get_product_availability_in_cart()
    assert product_availability_in_cart != None
    cart_page.click_button_checkout()
    checkout_page = CheckoutPage(driver)
    checkout_page.enter_first_name()
    checkout_page.enter_last_name()
    checkout_page.enter_zip_or_postal_code()
    checkout_page.click_button_continue()
    checkout_page.click_button_finish()
    text_from_checkout_complete = checkout_page.get_text_from_checkout_complete_container()
    assert "THANK YOU FOR YOUR ORDER" in text_from_checkout_complete

"""
1. Открыть браузер и развернуть на весь экран.
2. Перейти на сайт https://www.saucedemo.com/.
3. В поле ввода "Username" ввести "standard_user или performance_glitch_user".
4. В поле ввода "Password" ввести "secret_sauce".
5. Убедиться в авторизации пользователя.
6. Добавить два любых товар в корзину.
7. Перейти в корзину.
8. Убидиться что товар добавлен в корзину
9. Перейти к оформлению заказа.
10. Ввести имя,фамилию и почтовый код в поля ввода 'First Name','Last Name', 'Zip/Postal Code'
11. Нажать кнопку "Continue".
12. Получить цену товара.
13. Получить tax.
14. К стоимости товара прибавить tax.
15. Проверить что общая сумма равна сумме товара вместе с tax.
16. Закрыть браузер.
"""

def test_total_price(driver,user):
    driver.get("https://www.saucedemo.com/")
    authorization_page = AuthorizationPage(driver)
    authorization_page.enter_user_name(user)
    authorization_page.enter_password()
    authorization_page.click_login_button()
    get_current_url = driver.current_url
    assert get_current_url == current_url_after_authorization
    product_page = ProductPage(driver)
    product_page.add_to_card('2')
    product_page.add_to_card('3')
    product_page.go_to_shopping_cart()
    cart_page = CartPage(driver)
    product_availability_in_cart = cart_page.get_product_availability_in_cart()
    assert product_availability_in_cart != None
    cart_page.click_button_checkout()
    checkout_page = CheckoutPage(driver)
    checkout_page.enter_first_name()
    checkout_page.enter_last_name()
    checkout_page.enter_zip_or_postal_code()
    checkout_page.click_button_continue()
    item_total = checkout_page.get_item_total()
    tax = checkout_page.get_tax()
    total = checkout_page.get_total()
    assert (float(item_total) + float(tax)) == float(total)

"""
1. Открыть браузер и развернуть на весь экран.
2. Перейти на сайт https://www.saucedemo.com/.
3.В поле ввода "Username" ввести "standard_user или performance_glitch_user".
4.В поле ввода "Password" ввести "secret_sauce".
5.Убедиться в авторизации пользователя.
6. Определить количество элементов на странице.
7. Отсортировать список по цене (по возрастанию).
8. Запомнить первый элемент в списке.
9. Отсортировать список по цене в обратном порядке.
10. Убедиться, что сохраненный элемент находится в другом конце списка.
11. Закрыть браузер
"""

def test_sorting(driver,user):
    authorization_page = AuthorizationPage(driver)
    driver.get("https://www.saucedemo.com/")
    authorization_page.enter_user_name(user)
    authorization_page.enter_password()
    authorization_page.click_login_button()
    get_current_url = driver.current_url
    assert get_current_url == current_url_after_authorization
    product_page = ProductPage(driver)
    quantity_products_on_the_page = product_page.get_quantity_items_on_the_page()
    assert len(quantity_products_on_the_page) is 6
    product_page.open_sorting()
    product_page.click_sorting_low_to_high()
    name_first_product_on_the_page = product_page.get_name_first_item_on_the_page()
    product_page.open_sorting()
    product_page.click_sorting_high_to_low()
    name_last_product_on_the_page = product_page.get_name_last_item_on_the_page()
    assert name_first_product_on_the_page == name_last_product_on_the_page

"""
1. Открыть браузер и развернуть на весь экран.
2. Перейти на сайт https://www.saucedemo.com/.
3. В поле ввода "Username" ввести "standard_user или performance_glitch_user".
4. В поле ввода "Password" ввести "secret_sauce".
5. Убедиться в авторизации пользователя.
6. Добавить любой товар в корзину.
7. Нажать на кнопку бургер меню.
8. Нажать кнопку "LOGOUT".
9. Убедиться что пользователь разлогинен.
10. Авторизироваться тем же пользователем.
11. Перейти в корзину.
12. Убидиться ранее добавленный товар остался в корзине.
13. Закрыть браузер.
"""

def test_item_in_the_cart_after_re_authorization(driver,user):
    authorization_page = AuthorizationPage(driver)
    driver.get("https://www.saucedemo.com/")
    authorization_page.enter_user_name(user)
    authorization_page.enter_password()
    authorization_page.click_login_button()
    get_current_url = driver.current_url
    assert get_current_url == current_url_after_authorization
    product_page = ProductPage(driver)
    name_of_the_item_added_to_the_cart = product_page.get_name_first_item_on_the_page()
    product_page.add_to_card('1')
    product_page.open_burger_menu()
    product_page.logout()
    get_current_url = driver.current_url
    assert get_current_url != current_url_after_authorization
    authorization_page.enter_user_name(user)
    authorization_page.enter_password()
    authorization_page.click_login_button()
    get_current_url = driver.current_url
    assert get_current_url == current_url_after_authorization
    product_page.go_to_shopping_cart()
    cart_page = CartPage(driver)
    product_availability_in_cart = cart_page.get_product_availability_in_cart()
    assert product_availability_in_cart != None
    assert name_of_the_item_added_to_the_cart in product_availability_in_cart

"""
1. Открыть браузер и развернуть на весь экран.
2. Перейти на сайт https://www.saucedemo.com/
3. В поле ввода "Username" ввести "locked_out_user"
4. В поле ввода "Password" ввести "secret_sauce"
5. Убедиться что пользователь не авторизировался 
6. Убедиться что пользователю отобразился текст ошибки, по причине которой, пользователь не смог авторизоваться 
"""

def test_authorization_for_locked_out_user(driver):
    driver.get("https://www.saucedemo.com/")
    authorization_page = AuthorizationPage(driver)
    authorization_page.enter_user_name_for_locked_out_user()
    authorization_page.enter_password()
    authorization_page.click_login_button()
    get_current_url = driver.current_url
    assert get_current_url != current_url_after_authorization
    notification_error_text = authorization_page.get_notification_error_text()
    assert notification_error_text == "Epic sadface: Sorry, this user has been locked out."