from selenium.webdriver.common.by import By

INPUT_USERNAME = By.XPATH,"//input[@id='user-name']"
INPUT_PASSWORD = By.XPATH, "//input[@id='password']"
BUTTON_LOGIN = By.XPATH,"//input[@id='login-button']"
NOTIFICATION_ERROR = By.XPATH, '//H3[@data-test="error"]'