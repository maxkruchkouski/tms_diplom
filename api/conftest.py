import pytest
from random import randint
import randomaizer
from json import dumps, loads
import requests
from api_headers import HEADERS, HEADERS_AUTH
import api_urls


@pytest.fixture()
def auth():
    data = {
        "username": "admin",
        "password": "password123"
    }
    return data


@pytest.fixture()
def auth_token(auth):
    data = auth
    create_token_response = requests.post(
        url=api_urls.CREATE_TOKEN, headers=HEADERS, data=dumps(data)
    )
    HEADERS_AUTH.update({'Cookie': 'token=' + create_token_response.json()['token']})


@pytest.fixture()
def booking_id():
    get_booking_id_s_response = requests.get(
        url=api_urls.GET_BOOKING_IDS,
        headers=HEADERS
    )
    booking_id = loads(get_booking_id_s_response.text)
    assert get_booking_id_s_response.status_code == 200
    received_booking_id = str(booking_id[0]['bookingid'])
    return received_booking_id


@pytest.fixture()
def create_booking():
    random_data = randomaizer.RandomData()
    random_name = random_data.generate_word(6)
    random_word = random_data.generate_word(5)
    data = {
        "firstname": random_name,
        "lastname": random_name,
        "totalprice": randint(100, 300),
        "depositpaid": True,
        "bookingdates": {
            "checkin": "2020-08-09",
            "checkout": "2021-01-01"
        },
        "additionalneeds": random_word
    }
    return data


@pytest.fixture()
def partial_update_booking():
    data = {
        "firstname": "Test_First_Name",
        "lastname": "Test_Last_Name"
    }
    return data
