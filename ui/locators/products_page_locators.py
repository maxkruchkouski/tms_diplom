from selenium.webdriver.common.by import By

SHOPPING_CART = By.XPATH, '//a[@class="shopping_cart_link"]'
BUTTON_ADD_TO_CARD = By.XPATH,'(//button[@class="btn btn_primary btn_small btn_inventory"])'
BUTTON_ADD_TO_CARD_IN_THE_PRODUCT_CARD = By.XPATH,"//button[@id='add-to-cart-sauce-labs-backpack']"

def button_add_to_card(index:str):
    return (By.XPATH,f'(//button[@class="btn btn_primary btn_small btn_inventory"])[{index}]')
SORTING = By.XPATH,'//select[@class="product_sort_container"]'
SORTING_LOW_TO_HIGH = By.XPATH,'//select[@class="product_sort_container"]//option[@value="lohi"]'
SORTING_HIGH_LOW_TO = By.XPATH,'//select[@class="product_sort_container"]//option[@value="hilo"]'
QUANTITY_PRODUCTS_ON_THE_PAGE = By.XPATH,'//div[@class="inventory_item"]'
FIRST_PRODUCT_ON_THE_PAGE = By.XPATH,'(//div[@class="inventory_item_name"])[1]'
LAST_PRODUCT_ON_THE_PAGE = By.XPATH,'(//div[@class="inventory_item_name"])[6]'
BUTTON_BURGER_MENU = By.XPATH,"//button[@id='react-burger-menu-btn']"
BUTTON_LOGOUT = By.XPATH, "//a[@id='logout_sidebar_link']"