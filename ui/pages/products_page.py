from ui.locators import products_page_locators
from ui.base_page import BasePage
import time
class ProductPage(BasePage):

    def add_to_card(self,index:str):
        self.find_element(products_page_locators.button_add_to_card(index)).click()

    def go_to_shopping_cart(self):
        self.find_element(products_page_locators.SHOPPING_CART).click()

    def open_sorting(self):
        self.find_element(products_page_locators.SORTING).click()

    def click_sorting_low_to_high(self):
        self.find_element(products_page_locators.SORTING_LOW_TO_HIGH).click()

    def click_sorting_high_to_low(self):
        self.find_element(products_page_locators.SORTING_HIGH_LOW_TO).click()

    def get_quantity_items_on_the_page(self):
        return self.find_elements(products_page_locators.QUANTITY_PRODUCTS_ON_THE_PAGE)

    def get_name_first_item_on_the_page(self):
        return self.find_element(products_page_locators.FIRST_PRODUCT_ON_THE_PAGE).text

    def get_name_last_item_on_the_page(self):
        return self.find_element(products_page_locators.LAST_PRODUCT_ON_THE_PAGE).text

    def open_first_product_on_the_page(self):
        self.find_element(products_page_locators.FIRST_PRODUCT_ON_THE_PAGE).click()

    def click_add_to_cart_in_the_product_card(self):
        self.find_element(products_page_locators.BUTTON_ADD_TO_CARD_IN_THE_PRODUCT_CARD).click()

    def open_burger_menu(self):
        self.find_element(products_page_locators.BUTTON_BURGER_MENU).click()
        time.sleep(0.1)#TODO

    def logout(self):
        self.find_element(products_page_locators.BUTTON_LOGOUT).click()
