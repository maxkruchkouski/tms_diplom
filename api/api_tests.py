import requests
from api_headers import HEADERS, HEADERS_AUTH
import api_urls
from json import dumps


def test_health_check():
    health_check_response = requests.get(
        url=api_urls.HEALTH_CHECK
    )
    assert health_check_response.status_code == 201
    assert health_check_response.text == "Created"


def test_create_token(auth):
    create_token_response = requests.post(
        url=api_urls.CREATE_TOKEN, headers=HEADERS, data=dumps(auth)
    )
    assert create_token_response.status_code == 200
    assert 'token' in create_token_response.json()


def test_get_all_booking_ids():
    get_all_booking_ids_response = requests.get(
        url=api_urls.GET_BOOKING_IDS,
        headers=HEADERS,
    )
    assert get_all_booking_ids_response.status_code == 200
    assert "bookingid" in get_all_booking_ids_response.text


def test_get_booking_id_with_name(booking_id):
    get_booking_response = requests.get(
        url=api_urls.GET_BOOKING_IDS.format(id=booking_id),
        headers=HEADERS,
    )
    assert get_booking_response.status_code == 200
    assert 'firstname', 'lastname' in get_booking_response.json()


def test_create_booking(create_booking):
    create_booking_response = requests.post(
        url=api_urls.CREATE_BOOKING,
        headers=HEADERS,
        data=dumps(create_booking)
    )
    assert 'totalprice', 'bookingid' in create_booking_response.json()
    assert create_booking_response.status_code == 200


def test_update_booking(create_booking, booking_id, auth_token):
    update_data = {"firstname": "Test_Name", "lastname": 'Test_Lastname'}
    update_booking_data = {**create_booking, **update_data}
    updated_response = requests.put(
        url=api_urls.UPDATE_BOOKING.format(id=booking_id),
        headers=HEADERS_AUTH,
        data=dumps(update_booking_data)
    )
    assert updated_response.status_code == 200
    assert updated_response.json()['firstname'] == "Test_Name"
    assert updated_response.json()['lastname'] == "Test_Lastname"


def test_partial_update_booking(partial_update_booking, booking_id, auth_token):
    updated_booking_response = requests.patch(
        url=api_urls.UPDATE_BOOKING.format(id=booking_id),
        headers=HEADERS_AUTH,
        data=dumps(partial_update_booking),
    )

    assert updated_booking_response.status_code == 200
    assert updated_booking_response.json()['firstname'] == "Test_First_Name"
    assert updated_booking_response.json()['lastname'] == "Test_Last_Name"


def test_delete_booking(booking_id, auth_token):
    delete_booking_response = requests.delete(
        url=api_urls.DELETE_BOOKING.format(id=booking_id),
        headers=HEADERS_AUTH,

    )
    assert delete_booking_response.status_code == 201
    assert delete_booking_response.text == "Created"

    get_booking_response = requests.get(
        url=api_urls.DELETE_BOOKING.format(id=booking_id),
        headers=HEADERS_AUTH
    )
    assert get_booking_response.status_code == 404
