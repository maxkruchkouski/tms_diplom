from ui.locators import checkout_page_locators
from ui.base_page import BasePage

FIRST_NAME = "Test Name"
LAST_NAME = "Test Surname"
ZIP_OR_POSTAL_CODE = "123456"

class CheckoutPage(BasePage):

    def enter_first_name(self):
        self.find_element(checkout_page_locators.INPUT_FIRST_NAME).send_keys(FIRST_NAME)

    def enter_last_name(self):
        self.find_element(checkout_page_locators.INPUT_LAST_NAME).send_keys(LAST_NAME)

    def enter_zip_or_postal_code(self):
        self.find_element(checkout_page_locators.INPUT_ZIP_OR_POSTAL_CODE).send_keys(ZIP_OR_POSTAL_CODE)

    def click_button_continue(self):
        self.find_element(checkout_page_locators.BUTTON_CONTINUE).click()

    def click_button_finish(self):
        self.find_element(checkout_page_locators.BUTTON_FINISH).click()

    def get_text_from_checkout_complete_container(self):
        return self.find_element(checkout_page_locators.CHECKOUT_COMPLETE_CONTAINER).text

    def get_item_total(self):
        return self.find_element(checkout_page_locators.ITEM_TOTAL).text[13:]

    def get_tax(self):
        return self.find_element(checkout_page_locators.TAX).text[6:]

    def get_total(self):
        return self.find_element(checkout_page_locators.TOTAL).text[8:]