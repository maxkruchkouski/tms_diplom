from selenium.webdriver.common.by import By

INPUT_FIRST_NAME = By.XPATH,"//input[@id='first-name']"
INPUT_LAST_NAME = By.XPATH, "//input[@id='last-name']"
INPUT_ZIP_OR_POSTAL_CODE = By.XPATH,"//input[@id='postal-code']"
BUTTON_CONTINUE = By.XPATH,"//input[@id='continue']"
BUTTON_FINISH = By.XPATH,"//button[@id='finish']"
CHECKOUT_COMPLETE_CONTAINER = By.XPATH,"//div[@id='checkout_complete_container']"
ITEM_TOTAL = By.XPATH,'//div[@class="summary_subtotal_label"]'
TAX = By.XPATH, '//div[@class="summary_tax_label"]'
TOTAL = By.XPATH,'//div[@class="summary_total_label"]'