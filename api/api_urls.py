BASE_URL = 'https://restful-booker.herokuapp.com'

#AUTH
CREATE_TOKEN = BASE_URL + '/auth'

#BOOKING
GET_BOOKING_IDS = BASE_URL + '/booking'
CREATE_BOOKING = BASE_URL + '/booking'
UPDATE_BOOKING = BASE_URL + '/booking/{id}'
DELETE_BOOKING = BASE_URL + '/booking/{id}'

#PING
HEALTH_CHECK = BASE_URL + '/ping'