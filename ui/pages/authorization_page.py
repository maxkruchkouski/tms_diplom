from ui.locators import authorization_page_locators
from ui.base_page import BasePage

PASSWORD = 'secret_sauce'
LOCKED_OUT_USER = "locked_out_user"

class AuthorizationPage(BasePage):

    def enter_user_name(self,user):
        self.find_element(authorization_page_locators.INPUT_USERNAME).send_keys(user)

    def enter_user_name_for_locked_out_user(self):
        self.find_element(authorization_page_locators.INPUT_USERNAME).send_keys(LOCKED_OUT_USER)

    def enter_password(self):
        self.find_element(authorization_page_locators.INPUT_PASSWORD).send_keys(PASSWORD)

    def click_login_button(self):
        self.find_element(authorization_page_locators.BUTTON_LOGIN).click()

    def get_notification_error_text(self):
        return self.find_element(authorization_page_locators.NOTIFICATION_ERROR).text