from selenium.webdriver.common.by import By

CHECKOUT_BUTTON = By.XPATH,"//button[@id='checkout']"
PRODUCT_AVAILABILITY_IN_CART = (By.XPATH,'//div[@class="cart_item"]')