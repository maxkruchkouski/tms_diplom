from ui.locators import cart_page_locators
from ui.base_page import BasePage

class CartPage(BasePage):
    def click_button_checkout(self):
        self.find_element(cart_page_locators.CHECKOUT_BUTTON).click()

    def get_product_availability_in_cart(self):
        return self.find_element(cart_page_locators.PRODUCT_AVAILABILITY_IN_CART).text